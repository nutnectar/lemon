;;; lemon-memory.el --- Memory monitor for Lemon     -*- lexical-binding: t; -*-

;; Copyright (C) 2019, 2020, 2021  Ian Eure

;; Author: Ian Eure <ian@retrospec.tv>
;; Keywords: hardware

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(require 'lemon-monitor)

 ;; Linux

;;;###autoload
(defclass lemon-memory-linux (lemon-monitor-history)
  ((default-display-opts
     :initform '(:index "MEM:" :unit "%"))))

;;(cl-defmethod lemon-monitor-fetch ((_ lemon-memory-linux))
;;  (cl-destructuring-bind (memtotal memavailable memfree buffers cached)
;;      (lemon-monitor--linux-read-lines
;;       "/compat/linux/proc/meminfo" (lambda (str) (and str (read str)))
;;       '("MemTotal:" "MemAvailable:" "MemFree:" "Buffers:" "Cached:"))
;;    (if memavailable
;;        (/ (* (- memtotal memavailable) 100) memtotal)
;;      (/ (* (- memtotal (+ memfree buffers cached)) 100) memtotal))))

(cl-defmethod lemon-monitor-fetch ((_ lemon-memory-linux))
  (string-to-number (shell-command-to-string "freecolor -o | awk 'NR==2 {printf \"%.0f\", ($3 / $2) * 100; exit}'")))
  

(provide 'lemon-memory)
;;; lemon-memory.el ends here
